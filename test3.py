import matplotlib.pyplot as plt
import numpy as np
import scipy.stats as stats
import math
import statsmodels

ocjene = np.genfromtxt('baza.csv', delimiter=',', invalid_raise=False)


np.random.seed(148)

plt.figure(num=1, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
idx = np.random.randint(2000, size=200)
mu = np.mean(ocjene[idx,2])
variance = np.var(ocjene[idx,2] )
sigma = math.sqrt(variance)
n=stats.norm(scale=sigma,loc=mu)
print (statsmodels.stats.diagnostic.lilliefors(ocjene[idx,2]))
print(stats.kstest(ocjene[idx,2], n.cdf))
print (np.min(ocjene[idx,0]),np.min(ocjene[idx,1]),np.min(ocjene[idx,2]))
print (np.max(ocjene[idx,0]),np.max(ocjene[idx,1]),np.max(ocjene[idx,2]))
plt.scatter(ocjene[idx,1],ocjene[idx,2])
plt.ylabel("Ocjene IMdb(1-10)")
#plt.ylabel("Logaritam od Box Office US")
plt.xlabel("Ocjene Rotten Tomatoes")
plt.show()
plt.figure(num=5, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')

plt.scatter(ocjene[idx,1],ocjene[idx,0])
slope, intercept, r_value, p_value, std_err = stats.linregress(ocjene[idx,1],ocjene[idx,0])
plt.yscale("log")

#line = slope*x+ intercept
plt.xlim(0,100)
plt.ylim(10000,1e10)
#plt.plot(x, line)

plt.xlabel("Ocjene Rotten Tomatoes")
#plt.ylabel("Logaritam od Box Office US")
plt.ylabel("Zarada u dollarima")
plt
plt.show()

plt.figure(num=2, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
plt.hist(ocjene[idx,2],bins=range(11),edgecolor='black', linewidth=1.2)

#axes=plt.gca()
plt.xlim(0,10)
x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
plt.plot(x, stats.norm.pdf(x, mu, sigma)*200)
plt.ylabel("Broj ponavljanja")
plt.xlabel("IMdb Ocjena")



plt.show()

test = np.random.normal(loc=np.mean(ocjene[:,2]), scale=np.std(ocjene[:,2] ),size=10000)
plt.figure(num=3, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
plt.hist(ocjene[idx,1],bins=10)
plt.ylabel("Broj ponavljanja")
plt.xlabel("Rotten Tomatoes ocjene")
#print (np.log10(ocjene[idx,0]+(ocjene[idx,0]==0)))
plt.figure(num=4, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
plt.hist(np.log10(ocjene[idx,0]+(ocjene[idx,0]==0)),bins=10)
plt.ylabel("Broj ponavljanja")
plt.xlabel("logZarada")


plt.figure(num=6, figsize=(8, 6), dpi=80, facecolor='w', edgecolor='k')
idx = np.random.randint(2000, size=200)

plt.scatter(ocjene[idx,1],ocjene[idx,2],np.log10(ocjene[idx,0]+(ocjene[idx,0]==0)))
plt.ylabel("Ocjene IMdb(1-10)")
#plt.ylabel("Logaritam od Box Office US")
plt.xlabel("Ocjene Rotten Tomatoes")
np.savetxt("baza.csv", ocjene[idx], delimiter=",")
plt.show()